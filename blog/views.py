from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from blog.models import *
from django.http import JsonResponse
# Create your views here.
def home(request):
    if request.user.is_authenticated:
        blogs = Blog_Posts.objects.all()
        category = Blog_Category.objects.all()
        context = {'blogs':blogs,'category':category}
        return render(request, 'index.html', context)
    else:
        return redirect('login/')

def login_view(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        
        if user is not None:
            login(request, user)
            return redirect('home')
    return render(request, 'login.html')

def logout_view(request):
    logout(request)
    return redirect('home')

@login_required
def create_blog(request):
    if request.method == 'POST':
        # post_id = request.POST.get('post_id')
        post_title = request.POST.get('post_title')
        post_content = request.POST.get('post_content')
        category_name = request.POST.get('category_name')
        meta_title = request.POST.get('meta_title')
        meta_description = request.POST.get('meta_description')
        meta_keyword = request.POST.get('meta_keyword')
        post_title = request.POST.get('post_title')
        Blog_Posts.objects.create(
            title =post_title,
            content=post_content,
            category_id=Blog_Category.objects.get(category_name=category_name).category_id,
            meta_title =meta_title,
            meta_description = meta_description,
            meta_keywords=meta_keyword,
            created_by =request.user)
        return redirect('home')  # Replace 'home' with your desired redirect URL
    return render(request, 'create-blog.html')

def fetch_category(request):
    if request.method == 'GET':
        cat_id = request.POST.get('i')
        
        cat_name = Blog_Category.objects.get(category_id=cat_id).category_name
        # request.session['catid'] = cat_name
        return JsonResponse({'response':'Success', 'cat_name':cat_name})
    
def fetch_post(request, post_id):
    if request.method == 'GET':
        print(post_id)
        post = Blog_Posts.objects.filter(pk=post_id).values('title', 'content','category', 'meta_title')
        # print(post.query)
        # return JsonResponse({'response':list(post)})
        return render(request,'post.html',{'post':post})