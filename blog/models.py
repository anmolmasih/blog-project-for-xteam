from django.db import models
from django.contrib.auth.models import User

# Create your models for blog post.


class Blog_Category(models.Model):
    category_id = models.AutoField(primary_key=True)
    category_name = models.CharField(max_length=100)
    meta_title = models.CharField(max_length=100)
    meta_description = models.CharField(max_length=100)
    meta_keywords = models.CharField(max_length=200)
    def __str__(self):
        return self.category_name

class Blog_Posts(models.Model):
    post_id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=100)
    content = models.TextField()
    category = models.ForeignKey(Blog_Category, models.CASCADE)
    meta_title = models.CharField(max_length=100)
    meta_description = models.CharField(max_length=100, null=True)
    meta_keywords = models.CharField(max_length=200)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.title
