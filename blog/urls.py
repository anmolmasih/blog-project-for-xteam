from django.contrib import admin
from django.urls import path,include
from blog import views
from .views import logout_view

urlpatterns = [
    # path('admin/', admin.site.urls),
    path('',views.home, name='home'), # tell django to read urls.py in example app
    path('login/', views.login_view, name='login'),
    path('create-blog/', views.create_blog, name='create-blog'),
    path('fetch-category/', views.fetch_category, name='fetch-category'),
    path('post/<int:post_id>', views.fetch_post, name='fetch-category'),
    path('logout/', logout_view, name='logout'),

]
